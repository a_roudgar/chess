#include <iostream>
#include <string>
#include <math.h>
#include <conio.h>
#include <windows.h>

//mokhtasat tarif on, checker ra be surate araye [8][8] tarif kon, dafe avval tu main por kon.
using namespace std;

//COLORS LIST
//1: Blue
//2: Green
//3: Cyan
//4: Red
//5: Purple
//6: Yellow (Dark)
//7: Default white
//8: Gray/Grey
//9: Bright blue
//10: Brigth green
//11: Bright cyan
//12: Bright red
//13: Pink/Magenta
//14: Yellow
//15: Bright white
//Numbers after 15 include background colors

//	  background colors
//    0 = Black       8 = Gray
//    1 = Blue        9 = Light Blue
//    2 = Green       A = Light Green
//    3 = Aqua        B = Light Aqua
//    4 = Red         C = Light Red
//    5 = Purple      D = Light Purple
//    6 = Yellow      E = Light Yellow
//    7 = White       F = Bright White

typedef struct {
	int x;
	int y;
}place;

class checker
{
public:
	//color of each item
	char color;
	//an integer to indicate whether the cell is free or not
	int free;
	//declaring constructor
	checker();
	//id
	char id;
	//PAWN TURNCHECK
	int turnCheck;
};

class game
{
public:
	void show(checker[8][8]);
	void gotoxy(int, int);
	void hide_cursor();
	//set main board
	void start(checker[8][8]);
	//moving to new place
	void move(checker[8][8], place, place);
};

class emperor : public checker
{
private:
	place position;
public:
	//check if the destination is in range of move
	bool check_1(checker[8][8], place, place);
	//check if there is an obstacle in the middle
	bool check_2(checker[8][8], place, place);
	//check final
	bool check(checker[8][8], place, place);
	//check for being checked
	bool check_check(checker[8][8], place);
	//check for check mate
	bool check_mate(checker[8][8], place);
};

class pawn : public checker
{
private:
	place position;
public:
	//check if the destination is in range of move
	int check_1(checker[8][8], place, place);
	//check if there is an obstacle in the middle
	bool check_2(checker[8][8], place, place, int);
	//check final
	bool check(checker[8][8], place, place);
};

class knight : public checker
{
private:
	place position;
public:
	//check if the destination is in range of move
	bool check_1(checker[8][8], place, place);
	//check if there is an obstacle in the middle
	bool check_2(checker[8][8], place, place);
	//check final
	bool check(checker[8][8], place, place);
};

class queen : public checker
{
private:
	place position;
public:
	//check if the destination is in range of move
	bool check_1(checker[8][8], place, place);
	//check if there is an obstacle in the middle
	bool check_2(checker[8][8], place, place);
	//check final
	bool check(checker[8][8], place, place);
};

class bishop : public checker
{
private:
	place position;
public:
	//check if the destination is in range of move
	bool check_1(checker[8][8], place, place);
	//check if there is an obstacle in the middle
	bool check_2(checker[8][8], place, place);
	//check final
	bool check(checker[8][8], place, place);
};

class rook : public checker
{
private:
	place position;
public:
	//check if the destination is in range of move
	bool check_1(checker[8][8], place, place);
	//check if there is an obstacle in the middle
	bool check_2(checker[8][8], place, place);
	//check final
	bool check(checker[8][8], place, place);
};

//show the game board
void game::show(checker game_board[8][8])
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 2);
	int i, j;
	for (i = 0; i < 8; i++)
	{
		cout << "\t(" << char(i + 65) << ")";
	}
	cout << endl;
	for (i = 0; i < 8; i++)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 2);
		cout << endl << endl << "(" << i + 1 << ")";
		for (j = 0; j < 8; j++)
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), game_board[i][j].color == 'B' ? 3 : 7);
			game_board[i][j].free = game_board[i][j].id == ' ' ? true : false;
			cout << "\t " << game_board[i][j].id;
		}
		cout << endl;
	}
}

//change cursor position
void game::gotoxy(int x, int y)
{
	COORD p = { x, y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), p);
}

void game::start(checker game_board[8][8])
{
	rook r[4];
	bishop b[4];
	knight k[4];
	queen q[2];
	emperor e[2];
	pawn p[16];
	int i;
	for(i = 0; i < 8; i++)
	{
		for(int j = 0; j < 8; j++)
		{
			game_board[i][j].turnCheck = 0;
		}
	}
	//set empty spot
	for (i = 2; i < 6; i++) {
		for (int j = 0; j < 8; j++) {
			game_board[i][j].free = 1;
			game_board[i][j].id = ' ';
		}
	}
	//rook
	for (i = 0; i < 2; i++)
	{
		r[i].color = 'W';
		r[i].free = 0;
		r[i].id = 'R';
	}
	for (i = 2; i < 4; i++)
	{
		r[i].color = 'B';
		r[i].free = 0;
		r[i].id = 'R';
	}
	//bishop
	for (i = 0; i < 2; i++)
	{
		b[i].color = 'W';
		b[i].free = 0;
		b[i].id = 'B';
	}
	for (i = 2; i < 4; i++)
	{
		b[i].color = 'B';
		b[i].free = 0;
		b[i].id = 'B';
	}
	//knight
	for (i = 0; i < 2; i++)
	{
		k[i].color = 'W';
		k[i].free = 0;
		k[i].id = 'K';
	}
	for (i = 2; i < 4; i++)
	{
		k[i].color = 'B';
		k[i].free = 0;
		k[i].id = 'K';
	}
	//queen
	for (i = 0; i < 1; i++)
	{
		q[i].color = 'W';
		q[i].free = 0;
		q[i].id = 'Q';
	}
	for (i = 1; i < 2; i++)
	{
		q[i].color = 'B';
		q[i].free = 0;
		q[i].id = 'Q';
	}
	//emperor
	for (i = 0; i < 1; i++)
	{
		e[i].color = 'W';
		e[i].free = 0;
		e[i].id = 'E';
	}
	for (i = 1; i < 2; i++)
	{
		e[i].color = 'B';
		e[i].free = 0;
		e[i].id = 'E';
	}
	//pawn
	for (i = 0; i < 8; i++)
	{
		p[i].color = 'W';
		p[i].free = 0;
		p[i].id = 'P';
	}
	for (i = 8; i < 16; i++)
	{
		p[i].color = 'B';
		p[i].free = 0;
		p[i].id = 'P';
	}
	//set pawn
	for (i = 0; i < 8; i++)
	{
		game_board[1][i] = p[i];
	}
	for (i = 0; i < 8; i++)
	{
		game_board[6][i] = p[i + 8];
	}
	//set rook
	game_board[0][0] = r[0];
	game_board[0][7] = r[1];
	game_board[7][0] = r[2];
	game_board[7][7] = r[3];
	//set knight
	game_board[0][1] = k[0];
	game_board[0][6] = k[1];
	game_board[7][1] = k[2];
	game_board[7][6] = k[3];
	//set bishop
	game_board[0][2] = b[0];
	game_board[0][5] = b[1];
	game_board[7][2] = b[2];
	game_board[7][5] = b[3];
	//set emperor
	game_board[0][4] = e[0];
	game_board[7][4] = e[1];
	//set queen
	game_board[0][3] = q[0];
	game_board[7][3] = q[1];
}

//checker constructor
checker::checker()
{

}

//hide mouse cursor
void game::hide_cursor()
{
	HANDLE hOut;
	CONSOLE_CURSOR_INFO ConCurInf;

	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	ConCurInf.dwSize = 10;
	ConCurInf.bVisible = FALSE;

	SetConsoleCursorInfo(hOut, &ConCurInf);
}

bool rook::check_1(checker game_board[8][8], place from, place to)
{
	//if horizontal
	if(abs(to.y - from.y) == 0 && abs(to.x - from.x) > 0)
	{
		return 1;
	}
	//if vertical
	else if(abs(to.y - from.y) > 0 && abs(to.x - from.x) == 0)
	{
		return 1;
	}
	//if invalid move
	else
	{
		Beep(523, 500);
		return 0;
	}
}

bool rook::check_2(checker game_board[8][8], place from, place to)
{
	//if horizontal
	if(to.y - from.y == 0)
	{
		if(to.x == from.x)
		{
			Beep(523, 500);
			return 0;
		}
		else if(to.x > from.x)
		{
			//is there any obstacle
			for(int i = from.x + 1; i < to.x; i++)
			{
				if(game_board[from.y][i].free == 0)
				{
					Beep(523, 500);
					return 0;
				}
			}
			//is the destination free, if not is it friendly or not
			if(game_board[from.y][to.x].free == 0 && game_board[from.y][to.x].color == game_board[from.y][from.x].color)
			{
				Beep(523, 500);
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			//is there any obstacle
			for(int i = from.x - 1; i > to.x; i--)
			{
				if(game_board[from.y][from.x].free == 0)
				{
					Beep(523, 500);
					return 0;
				}
			}
			//is the destination free, if not is it friendly or not
			if(game_board[from.y][to.x].free == 0 && game_board[from.y][to.x].color == game_board[from.y][from.x].color)
			{
				Beep(523, 500);
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}
	//if vertical
	else
	{
		if(to.y == from.y)
		{
			Beep(523, 500);
			return 0;
		}	
		else if(to.y > from.y)
		{
			//is there any obstacle
			for(int i = from.y + 1; i < to.y; i++)
			{
				if(game_board[i][from.x].free == 0)
				{
					Beep(523, 500);
					return 0;
				}
			}
			//is the destination free, if not is it friendly or not
			if(game_board[to.y][from.x].free == 0 && game_board[to.y][from.x].color == game_board[from.y][from.x].color)
			{
				Beep(523, 500);
				return 0;
			}
			else
			{
				return 1;
			}
		}	
		else
		{
			//is there any obstacle
			for(int i = from.y - 1; i > to.y; i--)
			{
				if(game_board[i][from.x].free == 0)
				{
					Beep(523 ,500);
					return 0;
				}
			}
			//is the destination free, if not is it friendly or not
			if(game_board[to.y][from.x].free == 0 && game_board[to.y][from.x].color == game_board[from.y][from.x].color)
			{
				Beep(523, 500);
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}
}

bool bishop::check_1(checker game_board[8][8], place from, place to)
{
	if(abs(to.y - from.y) == abs(to.x - from.x))
	{
		return 1;
	}
	else
	{
		Beep(523, 500);
		return 0;
	}
}

bool bishop::check_2(checker game_board[8][8], place from, place to)
{
	//if there is any obstacle
	if(to.x > from.x && to.y > from.y)
	{
		for(int i = from.y + 1, j = from.x + 1; i < to.y && j < to.x; i++, j++)
		{
			if(game_board[i][j].free == 0)
			{
				Beep(523, 500);
				return 0;
			}
		}
	}
	else if(to.x < from.x && to.y > from.y)
	{
		for(int i = from.y + 1, j = from.x - 1; i < to.y && j > to.x; i++, j--)
		{
			if(game_board[i][j].free == 0)
			{
				Beep(523, 500);
				return 0;
			}	
		}
	}
	else if(to.x > from.x && to.y < from.y)
	{
		for(int i = from.y - 1, j = from.x + 1; i > to.y && j < to.x; i--, j++)
		{
			if(game_board[i][j].free == 0)
			{
				Beep(523, 500);
				return 0;
			}	
		}
	}
	else
	{
		for(int i = from.y - 1, j = from.x - 1; i < to.y && j < to.x; i--, j--)
		{
			if(game_board[i][j].free == 0)
			{
				Beep(523, 500);
				return 0;
			}	
		}
	}
	//is the destination free, if yes is it friendly or not
	if(game_board[to.y][to.x].color == game_board[from.y][from.x].color && game_board[to.y][to.x].free == 0)
	{
		Beep(523, 500);
		return 0;
	}
	else
	{
		return 1;
	}
}

int pawn::check_1(checker game_board[8][8], place from, place to) 
{
	
	if ( game_board[from.y][from.x].color == 'W')
	{
		if ((to.y - from.y) > 2 || abs(to.x - from.x) > 1 ){
			Beep(523, 500);
			return 0;
		}
		if ((to.y - from.y) == 2 && abs(to.x - from.x) == 0) {
			if (game_board[from.y][from.x].turnCheck == 0)
			{
				//if the player wants to move 2 vertically and has the permission
				game_board[from.y][from.x].turnCheck++;
				return 3;
			}
			
				
			else {
				Beep(523, 500);
				return 0;
			}
		}
		else if((to.y - from.y) == 1 && (to.x - from.x) == 0)
		{
			return 1;
		}
		else if(to.y - from.y == 1 && abs(to.x - from.x) == 1)
		{
			return 2;
		}
	}
	else  {
		if ((to.y - from.y) > 2 || abs(to.x - from.x) > 1 ){
			Beep(523, 500);
			return 0;
		}
		if ((to.y - from.y) == -2 && abs(to.x - from.x) == 0) {
			if (game_board[from.y][from.x].turnCheck == 0)
				return 1;
			else {
				Beep(523, 500);
				return 0;
			}
		}
		else if((to.y - from.y) == -1 && (to.x - from.x) == 0)
		{
			return 1;
		}
		else if(to.y - from.y == -1 && abs(to.x - from.x) == 1)
		{
			return 2;
		}	
	}
	
}

bool pawn::check_2(checker game_board[8][8] , place from , place to , int flag)
{
	//player wants tomove 2 vertically
	if(flag == 3){
		//obstacles in the way
		if(game_board[to.y - 1][to.x].free == 1 && game_board[to.y][to.x].free == 1 ){
			return 1;
		}
		else{
			Beep(523, 500);
			return 0;
		}
	}
	//move 1 vertically and 1 horizantally
	else if (flag == 2){
		if(game_board[to.y][to.x].free == 0 && game_board[to.y][to.x].color != game_board[from.y][from.x].color){
			return 1;
		}
	}
	//move 1 vertically
	else if (flag == 1){
		if (game_board[to.y][to.x].free == 1){
			return 1;
		}
		else{
			Beep(523, 500);
			return 0;
		}
	}
}

void game::move(checker game_board[8][8], place from, place to)
{
	if(game_board[to.y][to.x].free == 0)
	{
		game_board[to.y][to.x] = game_board[from.y][from.x];
		game_board[from.y][from.x].id = ' ';
		game_board[from.y][from.x].free = 1;
	}
	else
	{
		checker temp;
		temp = game_board[to.y][to.x];
		game_board[to.y][to.x] = game_board[from.y][from.x];
		game_board[from.y][from.x] = temp;
	}
}

bool knight::check_1(checker game_board[8][8] , place from , place to)
{
	if(abs(to.y - from.y) == 2 && abs(to.x - from.x) == 1){
		return 1;
	}
	else{
		Beep(523, 500);
		return 0;
	}
}

bool knight::check_2(checker game_board[8][8] , place from , place to)
{
	//checking for obstacle in destination
	if(game_board[to.y][to.x].free == 0 && game_board[to.y][to.x].color == game_board[from.y][from.x].color){
		Beep(523, 500);
		return 0;		
	}
	else{
		return 1;
	}
}

bool emperor::check_1(checker game_board[8][8], place from, place to)
{
	//if invalid
	if(abs(to.y - from.y > 1) || abs(to.x - from.x) > 1)
	{
		Beep(523, 500);
		return 0;
	}
	return 1;
}

bool emperor::check_2(checker game_board[8][8], place from, place to)
{
	//if friendly or not
	if(game_board[to.y][to.x].color == game_board[from.y][from.x].color)
	{
		Beep(523, 500);
		return 0;
	}
	//if the destination is adjacent to enemey emperor
	for(int i = 0; i < 8; i++)
	{
		for(int j = 0; j < 8; j++)
		{
			if(game_board[i][j].id == 'E' && game_board[i][j].color != game_board[from.y][from.x].color && abs(to.y - from.y) <= 1 && abs(to.x - from.x) <= 1)
			{
				Beep(523, 500);
				return 0;
			}
		}
	}
	return 1;
}

bool emperor::check_check(checker game_board[8][8] , place from)
{
	//returns 1 when it is not check and 0 when it is
	//checking up verticall
	int i,j;
	for(i = from.y +1 ; game_board[i][from.x].id == ' ' ; i++){
	}
	if(game_board[i][from.x].color != game_board[from.y][from.x].color){
		if(game_board[i][from.x].id == 'R' || game_board[i][from.x].id == 'Q'){
			Beep(523, 2000);
			return 0;			
		}
	}
	//checking down verticall
	for(i = from.y - 1 ; game_board[i][from.x].id == ' ' ; i--){
	}
	if(game_board[i][from.x].color != game_board[from.y][from.x].color){
		if(game_board[i][from.x].id == 'R' || game_board[i][from.x].id == 'Q'){
			Beep(523, 2000);
			return 0;			
		}
	}
	//checking right horizenticall
	for(i = from.x +1; game_board[from.y][i].id == ' ' ; i++){
	}
	if(game_board[from.y][i].color != game_board[from.y][from.x].color){
		if(game_board[from.y][i].id == 'R' || game_board[from.y][i].id == 'Q'){
			Beep(523, 2000);
			return 0;			
			}
		}
	//checking left horizenticall	
	for(i = from.x -1; game_board[from.y][i].id == ' ' ; i--){
	}
	if(game_board[from.y][i].color != game_board[from.y][from.x].color){
		if(game_board[from.y][i].id == 'R' || game_board[from.y][i].id == 'Q'){
			Beep(523, 2000);
			return 0;			
		}
	}
	//checking for pawns
	if(game_board[from.y +1][from.x +1].id == 'P' && game_board[from.y +1][from.x +1].color != game_board[from.y][from.x].color){
		Beep(523, 2000);
		return 0;		
	}
	if(game_board[from.y +1][from.x -1].id == 'P' && game_board[from.y +1][from.x -1].color != game_board[from.y][from.x].color){
		Beep(523, 2000);
		return 0;		
	}
	if(game_board[from.y -1][from.x -1].id == 'P' && game_board[from.y -1][from.x -1].color != game_board[from.y][from.x].color){
		Beep(523, 2000);
		return 0;		
	}
	if(game_board[from.y -1][from.x +1].id == 'P' && game_board[from.y -1][from.x +1].color != game_board[from.y][from.x].color){
		Beep(523, 2000);
		return 0;		
	}
	//checking for knights
	if(game_board[from.y +2][from.x +1].id == 'K' && game_board[from.y +2][from.x +1].color != game_board[from.y][from.x].color){
		Beep(523, 2000);
		return 0;		
	}
	if(game_board[from.y +2][from.x -1].id == 'K' && game_board[from.y +2][from.x -1].color != game_board[from.y][from.x].color){
		Beep(523, 2000);
		return 0;		
	}
	if(game_board[from.y -2][from.x +1].id == 'K' && game_board[from.y -2][from.x +1].color != game_board[from.y][from.x].color){
		Beep(523, 2000);
		return 0;		
	}
	if(game_board[from.y -2][from.x -1].id == 'K' && game_board[from.y -2][from.x -1].color != game_board[from.y][from.x].color){
		Beep(523, 2000);
		return 0;		
	}
	//CHECKING for bishop and queen
	//up-right
	for(i = from.x +1 , j = from.y +1; game_board[j][i].id == ' ' ; i++ , j++){
	}
	if(game_board[j][i].color != game_board[from.y][from.x].color){
		if(game_board[j][i].id == 'B' || game_board[j][i].id == 'Q'){
			Beep(523, 2000);
			return 0;			
			}
		}	
	//up-left
	for(i = from.x -1 , j = from.y +1; game_board[j][i].id == ' ' ; i-- , j++){
	}
	if(game_board[j][i].color != game_board[from.y][from.x].color){
		if(game_board[j][i].id == 'B' || game_board[j][i].id == 'Q'){
			Beep(523, 2000);
			return 0;			
			}
		}	
	//down-right
	for(i = from.x +1 , j = from.y -1; game_board[j][i].id == ' ' ; i++ , j--){
	}
	if(game_board[j][i].color != game_board[from.y][from.x].color){
		if(game_board[j][i].id == 'B' || game_board[j][i].id == 'Q'){
			Beep(523, 2000);
			return 0;			
			}
		}
	//down-left
	for(i = from.x -1 , j = from.y -1; game_board[j][i].id == ' ' ; i-- , j--){
	}
	if(game_board[j][i].color != game_board[from.y][from.x].color){
		if(game_board[j][i].id == 'B' || game_board[j][i].id == 'Q'){
			Beep(523, 2000);
			return 0;			
			}
		}
	return 1;	
}

bool emperor::check_mate(checker game_board[8][8] , place from)
{
	//creating an objrct of emperor
	emperor EMPEROR;
	//returns 1 when its not mate and 0 when it is
	//calling check_check for all the places around emperor
	from.x++;
	if((EMPEROR.check_check(game_board , from))== 0){
		Beep(523, 2000);
		return 0;
	}
	from.y++;
	if(EMPEROR.check_check(game_board , from) == 0){
		Beep(523, 2000);
		return 0;
	}
	from.x--;
	if(EMPEROR.check_check(game_board , from) == 0){
		Beep(523, 2000);
		return 0;
	}
	from.x--;
	if(EMPEROR.check_check(game_board , from) == 0){
		Beep(523, 2000);
		return 0;
	}
	from.y--;
	if(EMPEROR.check_check(game_board , from) == 0){
		Beep(523, 2000);
		return 0;
	}
	from.y--;
	if(EMPEROR.check_check(game_board , from) == 0){
		Beep(523, 2000);
		return 0;
	}
	from.x++;
	if(EMPEROR.check_check(game_board , from) == 0){
		Beep(523, 2000);
		return 0;
	}
	from.x++;
	if(EMPEROR.check_check(game_board , from) == 0){
		Beep(523, 2000);
		return 0;
	}
	return 1;
}

bool queen::check_1(checker game_board[8][8], place from, place to)
{
	//if horizontal
	if(abs(to.y - from.y) == 0 && abs(to.x - from.x) > 0)
	{
		return 1;
	}
	//if vertical
	if(abs(to.y - from.y) > 0 && abs(to.x - from.x) == 0)
	{
		return 1;
	}
	//if diagonal
	if(abs(to.y - from.y) == abs(to.x - from.x))
	{
		return 1;
	}
	//if invalid
	Beep(523, 500);
	return 0;	
}

bool queen::check_2(checker game_board[8][8], place from, place to)
{
	//if horizontal
	if(to.y - from.y == 0)
	{
		if(to.x == from.x)
		{
			Beep(523, 500);
			return 0;
		}
		else if(to.x > from.x)
		{
			//is there any obstacle
			for(int i = from.x + 1; i < to.x; i++)
			{
				if(game_board[from.y][i].free == 0)
				{
					Beep(523, 500);
					return 0;
				}
			}
			//is the destination free, if not is it friendly or not
			if(game_board[from.y][to.x].free == 0 && game_board[from.y][to.x].color == game_board[from.y][from.x].color)
			{
				Beep(523, 500);
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			//is there any obstacle
			for(int i = from.x - 1; i > to.x; i--)
			{
				if(game_board[from.y][from.x].free == 0)
				{
					Beep(523, 500);
					return 0;
				}
			}
			//is the destination free, if not is it friendly or not
			if(game_board[from.y][to.x].free == 0 && game_board[from.y][to.x].color == game_board[from.y][from.x].color)
			{
				Beep(523, 500);
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}
	//if vertical
	else if(to.x - from.x == 0)
	{
		if(to.y == from.y)
		{
			Beep(523, 500);
			return 0;
		}	
		else if(to.y > from.y)
		{
			//is there any obstacle
			for(int i = from.y + 1; i < to.y; i++)
			{
				if(game_board[i][from.x].free == 0)
				{
					Beep(523, 500);
					return 0;
				}
			}
			//is the destination free, if not is it friendly or not
			if(game_board[to.y][from.x].free == 0 && game_board[to.y][from.x].color == game_board[from.y][from.x].color)
			{
				Beep(523, 500);
				return 0;
			}
			else
			{
				return 1;
			}
		}	
		else
		{
			//is there any obstacle
			for(int i = from.y - 1; i > to.y; i--)
			{
				if(game_board[i][from.x].free == 0)
				{
					Beep(523 ,500);
					return 0;
				}
			}
			//is the destination free, if not is it friendly or not
			if(game_board[to.y][from.x].free == 0 && game_board[to.y][from.x].color == game_board[from.y][from.x].color)
			{
				Beep(523, 500);
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}
	//diagonal move
	else
	{
	//if there is any obstacle
	if(to.x > from.x && to.y > from.y)
	{
		for(int i = from.y + 1, j = from.x + 1; i < to.y && j < to.x; i++, j++)
		{
			if(game_board[i][j].free == 0)
			{
				Beep(523, 500);
				return 0;
			}
		}
	}
	else if(to.x < from.x && to.y > from.y)
	{
		for(int i = from.y + 1, j = from.x - 1; i < to.y && j > to.x; i++, j--)
		{
			if(game_board[i][j].free == 0)
			{
				Beep(523, 500);
				return 0;
			}	
		}
	}
	else if(to.x > from.x && to.y < from.y)
	{
		for(int i = from.y - 1, j = from.x + 1; i > to.y && j < to.x; i--, j++)
		{
			if(game_board[i][j].free == 0)
			{
				Beep(523, 500);
				return 0;
			}	
		}
	}
	else
	{
		for(int i = from.y - 1, j = from.x - 1; i < to.y && j < to.x; i--, j--)
		{
			if(game_board[i][j].free == 0)
			{
				Beep(523, 500);
				return 0;
			}	
		}
	}
	//is the destination free, if yes is it friendly or not
	if(game_board[to.y][to.x].color == game_board[from.y][from.x].color && game_board[to.y][to.x].free == 0)
	{
		Beep(523, 500);
		return 0;
	}
	else
	{
		return 1;
	}
	}	
}

bool rook::check(checker game_board[8][8], place from, place to)
{
	rook ROOK;
	if(ROOK.check_1(game_board, from, to))
	{
		return ROOK.check_2(game_board, from, to);
	}
	return 0;
}

bool bishop::check(checker game_board[8][8], place from, place to)
{
	bishop BISHOP;
	if(BISHOP.check_1(game_board, from, to))
	{
		return BISHOP.check_2(game_board, from, to);
	}	
	return 0;
}

bool queen::check(checker game_board[8][8], place from, place to)
{
	queen QUEEN;
	if(QUEEN.check_1(game_board, from, to))
	{
		return QUEEN.check_2(game_board, from, to);
	}	
	return 0;
}

bool knight::check(checker game_board[8][8], place from, place to)
{
	knight KNIGHT;
	if(KNIGHT.check_1(game_board, from, to))
	{
		return KNIGHT.check_2(game_board, from, to);
	}	
	return 0;
}

bool pawn::check(checker game_board[8][8], place from, place to)
{
	pawn PAWN;
	int flag = PAWN.check_1(game_board, from, to);
	if(flag)
	{
		return PAWN.check_2(game_board, from, to, flag);
	}	
	return 0;
}

bool emperor::check(checker game_board[8][8], place from, place to)
{
	emperor EMPEROR;
	if(EMPEROR.check_1(game_board, from, to))
	{
		return EMPEROR.check_2(game_board, from, to);
	}
	return 0;
}

int main()
{
	place from, to;
	//declare game object
	game GAME;
	//declare an 8 * 8 = 64 array object of class checker to control the game 
	checker game_board[8][8];
	GAME.start(game_board);
	int i, j;
	//initialize the amounts of game_board[i][j].color
	for (i = 0; i < 8; i++)
	{
		game_board[0][i].color = 'W';
		game_board[1][i].color = 'W';
		game_board[6][i].color = 'B';
		game_board[7][i].color = 'B';
		for (j = 2; j < 6; j++)
		{
			game_board[j][i].color = ' ';
		}
	}
	//hide mouse cursor
	GAME.hide_cursor();
	bool end_game = false;
	//creating objects
	pawn PAWN;
	rook ROOK;
	bishop BISHOP;
	queen QUEEN;
	knight KNIGHT;
	emperor EMPEROR;
	//emperors position
	place emperor_black;
	place emperor_white;
	emperor_black.y = 7;
	emperor_black.x = 4;
	emperor_white.y = 0;
	emperor_white.x = 4;
	//turns
	int turn = 0;
	//main game loop
	while (!end_game)
	{	
		GAME.show(game_board);
		cout << endl;
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 6);
		// get from
		cout << "Enter row = ";
		cin >> from.y;
		cout << "Enter column = ";
		cin >> from.x;
		cout << "*********************" << endl;
		//get to
		cout << "Enter destination row = ";
		cin >> to.y;
		cout << "Enter destination column = ";
		cin >> to.x;
		
		switch(game_board[from.y][from.x].id)
		{
			case 'P':	
				cout << "PAWN";
				if(PAWN.check(game_board, from, to))
				{
					cout << "IF";
					Beep(1000, 500);
					GAME.move(game_board, from, to);
					turn++;
				}
				if(PAWN.color == 'W' ? !EMPEROR.check_check(game_board, emperor_white) : !EMPEROR.check_check(game_board, emperor_black))
				{
					GAME.move(game_board, to, from);
					turn--;
				}
				break;
			case 'R':
				if(ROOK.check(game_board, from, to))
				{
					GAME.move(game_board, from, to);
					turn++;
				}
				if(ROOK.color == 'W' ? !EMPEROR.check_check(game_board, emperor_white) : !EMPEROR.check_check(game_board, emperor_black))
				{
					GAME.move(game_board, to, from);
					turn--;
				}
				break;
			case 'B':
				if(BISHOP.check(game_board, from, to))
				{
					GAME.move(game_board, from, to);
					turn++;
				}
				if(BISHOP.color == 'W' ? !EMPEROR.check_check(game_board, emperor_white) : !EMPEROR.check_check(game_board, emperor_black))
				{
					GAME.move(game_board, to, from);
					turn--;
				}
				break;
			case 'Q':
				if(QUEEN.check(game_board, from, to))
				{
					GAME.move(game_board, from, to);
					turn++;
				}
				if(QUEEN.color == 'W' ? !EMPEROR.check_check(game_board, emperor_white) : !EMPEROR.check_check(game_board, emperor_black))
				{
					GAME.move(game_board, to, from);
					turn--;
				}
				break;
			case 'K':
				cout << "KNIGHT";
				if(KNIGHT.check(game_board, from, to))
				{
					GAME.move(game_board, from, to);
					turn++;
				}
				if(KNIGHT.color == 'W' ? !EMPEROR.check_check(game_board, emperor_white) : !EMPEROR.check_check(game_board, emperor_black))
				{
					GAME.move(game_board, to, from);
					turn--;
				}
				break;
			case 'E':
				if(EMPEROR.check(game_board, from, to))
				{
					turn++;
					GAME.move(game_board, from, to);
					if(EMPEROR.color == 'W')
					{
						emperor_white = to;
					}
					else
					{
						emperor_black = to;
					}
				}
				if(EMPEROR.color == 'W' ? !EMPEROR.check_check(game_board, emperor_white) : !EMPEROR.check_check(game_board, emperor_black))
				{
					GAME.move(game_board, to, from);
					turn--;
				}
				break;
			
		}
		cout << "\nEOF Switch";
		if(turn % 2 == 1)
		{
			if(EMPEROR.check_check(game_board, emperor_black))
			{
				if(EMPEROR.check_mate(game_board, emperor_black))
				{
					end_game = true;
					system("CLS");
					cout << "White Won!";
				}
			}
		}
		else
		{
			if(EMPEROR.check_check(game_board, emperor_white))
			{
				if(EMPEROR.check_mate(game_board, emperor_white))
				{
					end_game = true;
					system("CLS");
					cout << "Black Won!";
				}
			}	
		}
		cout << "\nEOF While";
		GAME.gotoxy(0, 0);
		
	}
	cout << "\nEOF Game";
	return 0;
}
