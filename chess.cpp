#include <iostream>
#include <string>
#include <conio.h>
#include <windows.h>
//mokhtasat tarif on, checker ra be surate araye [8][8] tarif kon, dafe avval tu main por kon.
using namespace std;

//COLORS LIST
//1: Blue
//2: Green
//3: Cyan
//4: Red
//5: Purple
//6: Yellow (Dark)
//7: Default white
//8: Gray/Grey
//9: Bright blue
//10: Brigth green
//11: Bright cyan
//12: Bright red
//13: Pink/Magenta
//14: Yellow
//15: Bright white
//Numbers after 15 include background colors

//	  background colors
//    0 = Black       8 = Gray
//    1 = Blue        9 = Light Blue
//    2 = Green       A = Light Green
//    3 = Aqua        B = Light Aqua
//    4 = Red         C = Light Red
//    5 = Purple      D = Light Purple
//    6 = Yellow      E = Light Yellow
//    7 = White       F = Bright White

typedef struct{
	int x;
	int y;
}place;

class checker
{
	public:
		//color of each item
		char color;
		//an array of game to show to the gamer
		string board[8][8];
		//an integer to indicate whether the cell is free or not
		int free;
		//declaring constructor
		checker();
};

class game
{
	public:
		void show(checker [8][8]);	
		void gotoxy(int, int);
		void hide_cursor();
		//set main board
		void start(checker [8][8]);
};

class king : public checker
{
	private:
		place position;
	public:
		//check if the destination is in range of move
		bool check_1(checker [8][8], place, place);
		//check if there is an obstacle in the middle
		bool check_2(checker [8][8], place, place);	
		//check if the friendly king gets checked
		bool check_3(checker [8][8], place, place);
		//moving to new place
		void move(checker [8][8], place, place);
};

class pawn : public checker
{
	private:
		place position;
	public:
		//check if the destination is in range of move
		bool check_1(checker [8][8], place, place);
		//check if there is an obstacle in the middle
		bool check_2(checker [8][8], place, place);	
		//check if the friendly king gets checked
		bool check_3(checker [8][8], place, place);		
		//moving to new place
		void move(checker [8][8], place, place);
};

class knight : public checker
{
	private:
		place position;
	public:
		//check if the destination is in range of move
		bool check_1(checker [8][8], place, place);
		//check if there is an obstacle in the middle
		bool check_2(checker [8][8], place, place);	
		//check if the friendly king gets checked
		bool check_3(checker [8][8], place, place);		
		//moving to new place
		void move(checker [8][8], place, place);
};

class queen : public checker
{
	private:
		place position;
	public:
		//check if the destination is in range of move
		bool check_1(checker [8][8], place, place);
		//check if there is an obstacle in the middle
		bool check_2(checker [8][8], place, place);	
		//check if the friendly king gets checked
		bool check_3(checker [8][8], place, place);	
		//moving to new place
		void move(checker [8][8], place, place);
};

class bishop : public checker
{
	private:
		place position;
	public:
		//check if the destination is in range of move
		bool check_1(checker [8][8], place, place);
		//check if there is an obstacle in the middle
		bool check_2(checker [8][8], place, place);	
		//check if the friendly king gets checked
		bool check_3(checker [8][8], place, place);
		//moving to new place
		void move(checker [8][8], place, place);
};

class rook : public checker
{
	private:
		place position;
	public:
		//check if the destination is in range of move
		bool check_1(checker [8][8], place, place);
		//check if there is an obstacle in the middle
		bool check_2(checker [8][8], place, place);	
		//check if the friendly king gets checked
		bool check_3(checker [8][8], place, place);
		//moving to new place
		void move(checker [8][8], place, place);
};

//show the game board
void game::show(checker game_board[8][8])
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 2);
	int i, j;
	for(i = 0; i < 8; i++)
	{
		cout << "\t(" << char(i + 65) << ")";
	}
	cout << endl;
	for(i = 0; i < 8; i++)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 2);
		cout << endl << endl << "(" << i + 1 << ")";
		for(j = 0; j < 8; j++)
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), game_board[i][j].color == 'B' ? 3 : 7);
			game_board[i][j].free = game_board[i][j].board[i][j] == "" ? true : false;
			cout << "\t " << game_board[i][j].board[i][j][1];
		}
		cout << endl;
	}
}

//change cursor position
void game::gotoxy(int x, int y)
{
    COORD p = { x, y };
    SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), p );
}

void game::start(checker game_board[8][8])
{
	rook r[4];
	bishop b[4];
	knight k[4];
	queen q[2];
	king e[2];
	pawn p[16];
	int i;
	//rook
	for(i = 0; i < 2; i++)
	{
		r[i].color = 'W';
		r[i].free = 0;
	}
	for(i = 2; i < 4; i++)
	{
		r[i].color = 'B';
		r[i].free = 0;
	}
	//bishop
	for(i = 0; i < 2; i++)
	{
		b[i].color = 'W';
		b[i].free = 0;
	}
	for(i = 2; i < 4; i++)
	{
		b[i].color = 'B';
		b[i].free = 0;
	}
	//knight
	for(i = 0; i < 2; i++)
	{
		k[i].color = 'W';
		k[i].free = 0;
	}
	for(i = 2; i < 4; i++)
	{
		k[i].color = 'B';
		k[i].free = 0;
	}
	//queen
	for(i = 0; i < 1; i++)
	{
		q[i].color = 'W';
		q[i].free = 0;
	}
	for(i = 1; i < 2; i++)
	{
		q[i].color = 'B';
		q[i].free = 0;
	}
	//emperor
	for(i = 0; i < 1; i++)
	{
		e[i].color = 'W';
		e[i].free = 0;
	}
	for(i = 1; i < 2; i++)
	{
		e[i].color = 'B';
		e[i].free = 0;
	}
	//pawn
	for(i = 0; i < 8; i++)
	{
		p[i].color = 'W';
		p[i].free = 0;
	}
	for(i = 8; i < 16; i++)
	{
		p[i].color = 'B';
		p[i].free = 0;
	}
	//set pawn
	for ( i = 0 ; i < 8 ; i++ )
	{
		game_board[1][i] = p[i];
	}
	for ( i = 0 ; i < 8 ; i++ )
	{
		game_board[6][i] = p[i + 8];
	}
	//set rook
	game_board[0][0] = r[0];
	game_board[0][7] = r[1];
	game_board[7][0] = r[2];
	game_board[7][7] = r[3];
	//set knight
	game_board[0][1] = k[0];
	game_board[0][6] = k[1];
	game_board[7][1] = k[2];
	game_board[7][6] = k[3];
	//set bishop
	game_board[0][2] = b[0];
	game_board[0][5] = b[1];
	game_board[7][2] = b[2];
	game_board[7][5] = b[3];
	//set emperor
	game_board[0][4] = e[0];
	game_board[7][4] = e[1];
	//set queen
	game_board[0][3] = q[0];
	game_board[7][3] = q[1];
}

//checker constructor
checker::checker()
{
	int i, j;
	//initializing white positions
	//'Z' stands for king, 'K' stands for knight
	board[0][0] = "WR";
	board[0][7] = "WR";
	board[0][1] = "WK";
	board[0][6] = "WK";
	board[0][2] = "WB";
	board[0][5] = "WB";
	board[0][3] = "WQ";
	board[0][4] = "WE";
	for(i = 0; i < 8; i++)
	{
		board[1][i] = "WP";
	}
	//initializing black positions
	//'Z' stands for king, 'K' stands for knight
	board[7][0] = "BR";
	board[7][7] = "BR";
	board[7][1] = "BK";
	board[7][6] = "BK";
	board[7][2] = "BB";
	board[7][5] = "BB";
	board[7][3] = "BQ";
	board[7][4] = "BE";
	for(i = 0; i < 8; i++)
	{
		board[6][i] = "BP";
	}
	//setting other sells to empty string
	for(i = 2; i < 6; i++)
	{
		for(j = 0; j < 8; j++)
		{
			board[i][j] = "";
		}
		
	}
	
}

//hide mouse cursor
void game::hide_cursor()
{
	HANDLE hOut;
	CONSOLE_CURSOR_INFO ConCurInf;

	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	ConCurInf.dwSize = 10;
	ConCurInf.bVisible = FALSE;

	SetConsoleCursorInfo(hOut, &ConCurInf);
}

int main()
{
	//declare game object
	game GAME;
	//declare an 8 * 8 = 64 array object of class checker to control the game 
	checker game_board[8][8];

	
	int i, j;
	//initialize the amounts of game_board[i][j].color
	for(i = 0; i < 8; i++)
	{
		game_board[0][i].color = 'W';
		game_board[1][i].color = 'W';
		game_board[6][i].color = 'B';
		game_board[7][i].color = 'B';
		for(j = 2; j < 6; j++)
		{
			game_board[j][i].color = ' ';
		}
	}
	//hide mouse cursor
	GAME.hide_cursor();
	bool end_game = false;
	while(!end_game)
	{
		GAME.show(game_board);
		//system("CLS");
		GAME.gotoxy(0, 0);
	}
	getch();
	return 0;
}
